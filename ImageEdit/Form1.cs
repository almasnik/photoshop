﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEdit
{
    public partial class Form1 : Form
    {

        Image loadedImage = null;
        Bitmap img = null;

        String[] chars = { "█", "#", "@", "%", "=", "+", "*", ":", "-", ".", " " };

        public Form1()
        {
            InitializeComponent();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            loadedImage = Image.FromFile(openFileDialog1.FileName);
            img = new Bitmap(loadedImage);
            LoadImage(img);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void grayScaleButton_Click(object sender, EventArgs e)
        {
            LoadImage(toGrayscale(img));
        }

        private void contrastBar_ValueChanged(object sender, EventArgs e)
        {
            LoadImage(Changebrightness(img));
        }

        private void asciiArt_Click(object sender, EventArgs e)
        {
            convertToAscii(img);
        }

        private void LoadImage(Bitmap image)
        {
            img = image;
            pictureBox1.Image = image;
        }

        private Bitmap toGrayscale(Bitmap input)
        {

            Bitmap output = new Bitmap(input.Width, input.Height);

            for (int x = 0; x< input.Width; x++)
            {
                for (int y = 0; y < input.Height; y++)
                {
                    Color pixelColor = input.GetPixel(x, y);
                    int avg = (pixelColor.R + pixelColor.G +pixelColor.B) / 3;
                    Color newColor = Color.FromArgb(avg, avg, avg);
                    output.SetPixel(x, y, newColor);
                }
            }

            return output;
        }

        private Bitmap Changebrightness(Bitmap input)
        {
            Bitmap output = new Bitmap(input.Width, input.Height);

            for (int x = 0; x < input.Width; x++)
            {
                for (int y = 0; y < input.Height; y++)
                {
                    Color pixelColor = input.GetPixel(x, y);
                    Color newColor = Color.FromArgb( pixelColor.R + brightnessBar.Value, pixelColor.G + brightnessBar.Value, pixelColor.B + brightnessBar.Value);
                    output.SetPixel(x, y, newColor);
                }
            }

            return output;
        }

        private void convertToAscii(Bitmap input)
        {
            string art = "";

            for (int y = 0; y < input.Height; y+=3)
            {
                for (int x = 0; x < input.Width; x+=3)
                {
                    Color pixelColor = input.GetPixel(x, y);
                    int avg = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;

                    art += chars[(avg * chars.Length-1) / 255];
                    art += chars[(avg * chars.Length-1) / 255];
                }
                art += "\n";
            }

            File.WriteAllText(@"C:\Users\Bandit\Desktop\AsciiImage.txt", art);
        }
    }
}
